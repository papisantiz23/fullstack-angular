const express = require('express')
const passprot = require('passport')
const controller = require('../controllers/position') 
const router = express.Router()

router.get('/:id', passprot.authenticate('jwt', {session:false}), controller.getCategoryId )
router.post('/', passprot.authenticate('jwt', {session:false}), controller.create )
router.patch('/:id', passprot.authenticate('jwt', {session:false}), controller.create )
router.delete('/:id', passprot.authenticate('jwt', {session:false}), controller.remove )

 

module.exports = router 