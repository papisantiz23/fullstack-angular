const express = require('express')
const passprot = require('passport')
const upload = require('../middleware/upload')
const controller = require('../controllers/category') 
const router = express.Router()

router.get('/', passprot.authenticate('jwt', {session:false}), controller.getAll )
router.get('/:id', passprot.authenticate('jwt', {session:false}), controller.getById )
router.delete('/:id', passprot.authenticate('jwt', {session:false}), controller.remove )
router.post('/', passprot.authenticate('jwt', {session:false}), upload.single('image'), controller.create )
router.patch('/:id', passprot.authenticate('jwt', {session:false}), upload.single('image'), controller.update )

 

module.exports = router 