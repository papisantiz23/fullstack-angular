const express = require('express')
const passprot = require('passport')
const controller = require('../controllers/order') 
const router = express.Router()

router.get('/', passprot.authenticate('jwt', {session:false}), controller.getAll )
router.post('/', passprot.authenticate('jwt', {session:false}), controller.create )

 

module.exports = router 